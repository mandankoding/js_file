let stars = document.getElementsByClassName('card-star');

for (let i = 0; i < stars.length; i++) {
    stars[i].addEventListener('mouseover', function () {
        let onStar = parseInt(this.dataset.value, 10);
        let thisStars = this.parentElement.querySelectorAll('.card-star');
        thisStars.forEach(e => {
            let targetValue = Number(e.dataset.value);
            if (targetValue <= onStar) {
                e.classList.add('hover');
            } else {
                e.classList.remove('hover');
            }
        })
    });

    stars[i].addEventListener('mouseout', function () {
        let thisStars = this.parentElement.querySelectorAll('.card-star');
        thisStars.forEach(e => {
            e.classList.remove('hover');
        })
    });

    stars[i].addEventListener('click', function (e) {
        e.preventDefault();
        let onStar = parseInt(this.dataset.value, 10);
        let thisStars = this.parentElement.querySelectorAll('.card-star');
        thisStars.forEach(e => {
            e.classList.remove('selected');
        })
        for (let i = 0; i < onStar; i++) {
            thisStars[i].classList.add('selected');
            e.stopPropagation();
        }
        document.addEventListener('click', function (e) {
            if (e.target !== thisStars) {
                thisStars.forEach(e => {
                    e.classList.remove('selected');
                })
            }
        });
    });
};
